package org.mybatis.generator.extend.codegen.mybatis3.xmlmapper;

import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Element;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.codegen.mybatis3.xmlmapper.XMLMapperGenerator;
import org.mybatis.generator.extend.codegen.mybatis3.xmlmapper.elements.BatchDeleteElementGenerator;
import org.mybatis.generator.extend.codegen.mybatis3.xmlmapper.elements.BatchInsertElementGenerator;
import org.mybatis.generator.extend.codegen.mybatis3.xmlmapper.elements.BatchUpdateElementGenerator;
import org.mybatis.generator.extend.codegen.mybatis3.xmlmapper.elements.CountElementGenerator;
import org.mybatis.generator.extend.codegen.mybatis3.xmlmapper.elements.SelectAllElementGenerator;
import org.mybatis.generator.extend.codegen.mybatis3.xmlmapper.elements.SelectByConditionsElementGenerator;
import org.mybatis.generator.extend.codegen.mybatis3.xmlmapper.elements.SelectByEntityElementGenerator;
import org.mybatis.generator.extend.codegen.mybatis3.xmlmapper.elements.SelectByPrimaryKeysElementGenerator;
import org.mybatis.generator.extend.codegen.mybatis3.xmlmapper.elements.UpdateByPrimaryKeyElementGenerator;
import org.mybatis.generator.extend.codegen.mybatis3.xmlmapper.elements.UpdateByPrimaryKeySelectiveElementGenerator;

import java.util.List;

/**
 * Created by Administrator on 2017/1/8.
 */
public class ExtendXMLMapperGenerator extends XMLMapperGenerator {

    @Override
    protected XmlElement getSqlMapElement() {
        XmlElement sqlMapElement = super.getSqlMapElement();

//        //删除 selectByExampleWithBLOBs
//        removeElements(sqlMapElement, "select", "selectByExampleWithBLOBs");

//        //删除 selectByPrimaryKey deleteByPrimaryKey
//        removeElements(sqlMapElement, "select", "selectByPrimaryKey");
//        removeElements(sqlMapElement, "delete", "deleteByPrimaryKey");
//        removeElements(sqlMapElement, "insert", "insert");
//        removeElements(sqlMapElement, "insert", "insertSelective");
//        removeElements(sqlMapElement, "update", "updateByPrimaryKeySelective");
//        removeElements(sqlMapElement, "update", "updateByPrimaryKey");

        // 查询所有
        addSelectAllElement(sqlMapElement);
//        // 根据多个主键查询
//        addSelectByPrimaryKeysElement(sqlMapElement);
//        // 实体查询
//        addSelectByEntityElement(sqlMapElement);
//        // 自定义条件查询
//        addSelectByConditionsElement(sqlMapElement);
//        // 统计
//        addCountElement(sqlMapElement);
//        // 批量插入
//        addBatchInsertElement(sqlMapElement);
//        // 批量更新
//        addBatchUpdateElement(sqlMapElement);
//        // 批量删除
//        addBatchDeleteElement(sqlMapElement);
        // 更新已选
        updateByPrimaryKeySelectiveElement(sqlMapElement);
        // 更新
        updateByPrimaryKeyElement(sqlMapElement);

        return sqlMapElement;
    }

    protected void addSelectByPrimaryKeysElement(XmlElement parentElement) {
        initializeAndExecuteGenerator(new SelectByPrimaryKeysElementGenerator(), parentElement);
    }

    protected void addSelectByConditionsElement(XmlElement parentElement) {
        initializeAndExecuteGenerator(new SelectByConditionsElementGenerator(), parentElement);
    }

    protected void addBatchDeleteElement(XmlElement parentElement) {
        initializeAndExecuteGenerator(new BatchDeleteElementGenerator(), parentElement);
    }

    protected void addBatchUpdateElement(XmlElement parentElement) {
        initializeAndExecuteGenerator(new BatchUpdateElementGenerator(), parentElement);
    }

    protected void addBatchInsertElement(XmlElement parentElement) {
        initializeAndExecuteGenerator(new BatchInsertElementGenerator(), parentElement);
    }

    protected void addCountElement(XmlElement parentElement) {
        initializeAndExecuteGenerator(new CountElementGenerator(), parentElement);
    }

    protected void addSelectByEntityElement(XmlElement parentElement) {
        initializeAndExecuteGenerator(new SelectByEntityElementGenerator(), parentElement);
    }

    protected void addSelectAllElement(XmlElement parentElement) {
        initializeAndExecuteGenerator(new SelectAllElementGenerator(), parentElement);
    }

    protected void updateByPrimaryKeySelectiveElement(XmlElement parentElement) {
        initializeAndExecuteGenerator(new UpdateByPrimaryKeySelectiveElementGenerator(), parentElement);
    }

    protected void updateByPrimaryKeyElement(XmlElement parentElement) {
        initializeAndExecuteGenerator(new UpdateByPrimaryKeyElementGenerator(), parentElement);
    }

    //自定义代码
    private void removeElements(XmlElement parentElement, String updateSql, String updateId) {
        List<Element> elementList = parentElement.getElements();
        int num = -1;
        for (int i = 0; i < elementList.size(); i++) {
            Element element = elementList.get(i);
            String updateName = ((XmlElement) element).getName();
            if (updateSql.equals(updateName)) {
                List<Attribute> attributeList = ((XmlElement) element).getAttributes();
                for (Attribute attribute : attributeList) {
                    if (updateId.equals(attribute.getValue())) {
                        num = i;
                    }
                }
            }
        }
        if (num > 0) {
            elementList.remove(num);
        }
    }
}
