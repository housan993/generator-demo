package org.mybatis.generator.extend.codegen.mybatis3.xmlmapper.elements;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Element;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.codegen.ibatis2.Ibatis2FormattingUtilities;
import org.mybatis.generator.codegen.mybatis3.xmlmapper.elements.AbstractXmlElementGenerator;

import java.util.List;

/**
 * Created by Administrator on 2017/1/8.
 */
public class UpdateByPrimaryKeyElementGenerator extends AbstractXmlElementGenerator {
    @Override
    public void addElements(XmlElement parentElement) {
        //删除 updateByPrimaryKey
        removeElements(parentElement, "update", "updateByPrimaryKey");

        //新增 updateByPrimaryKey
        XmlElement answer = new XmlElement("update");
        answer.addAttribute(new Attribute("id", "updateByPrimaryKey"));
        FullyQualifiedJavaType parameterType = introspectedTable.getRules().calculateAllFieldsClass();
        answer.addAttribute(new Attribute("parameterType", parameterType.getFullyQualifiedName()));
        context.getCommentGenerator().addComment(answer);

        StringBuilder sb = new StringBuilder();
        sb.append("update ");
        sb.append(introspectedTable.getFullyQualifiedTableNameAtRuntime());
        answer.addElement(new TextElement(sb.toString()));

        addTextElement(answer);

        addPrimaryKeyXmlElement(answer);

        parentElement.addElement(answer);
    }

    //自定义代码
    private void removeElements(XmlElement parentElement, String updateSql, String updateId) {
        List<Element> elementList = parentElement.getElements();
        int num = -1;
        for (int i = 0; i < elementList.size(); i++) {
            Element element = elementList.get(i);
            String updateName = ((XmlElement) element).getName();
            if (updateSql.equals(updateName)) {
                List<Attribute> attributeList = ((XmlElement) element).getAttributes();
                for (Attribute attribute : attributeList) {
                    if (updateId.equals(attribute.getValue())) {
                        num = i;
                    }
                }
            }
        }
        if (num > 0) {
            elementList.remove(num);
        }
    }

    public void addTextElement(XmlElement answer) {
        StringBuilder sb = new StringBuilder();
        sb.append("set");
        answer.addElement(new TextElement(sb.toString()));

        List<IntrospectedColumn> nonPrimaryKeyColumns = introspectedTable.getNonPrimaryKeyColumns();
        int index = 0;
        for (IntrospectedColumn column : nonPrimaryKeyColumns) {
            String property = column.getJavaProperty();
            sb.setLength(0);

            if ("version" .equalsIgnoreCase(Ibatis2FormattingUtilities.getEscapedColumnName(column))) {
                sb.append(Ibatis2FormattingUtilities.getEscapedColumnName(column));
                sb.append(" = ");
                sb.append(Ibatis2FormattingUtilities.getEscapedColumnName(column) + " + 1");
            } else {
                sb.append(Ibatis2FormattingUtilities.getEscapedColumnName(column));
                sb.append(" = ");
                sb.append("#{" + property + ", jdbcType=" + column.getJdbcTypeName() + "}");
            }
            index++;
            if (index < nonPrimaryKeyColumns.size()) {
                sb.append(",");
            }
            answer.addElement(new TextElement(sb.toString()));
        }
    }

    public void addPrimaryKeyXmlElement(XmlElement answer) {
        StringBuilder sb = new StringBuilder();
        boolean and = false;
        for (IntrospectedColumn column : introspectedTable.getPrimaryKeyColumns()) {
            sb.setLength(0);
            if (and) {
                sb.append("  and ");
            } else {
                sb.append("where ");
                and = true;
            }

            sb.append(Ibatis2FormattingUtilities.getEscapedColumnName(column));
            sb.append(" = ");
            sb.append("#{" + column.getJavaProperty() + ", jdbcType=" + column.getJdbcTypeName() + "}");
            answer.addElement(new TextElement(sb.toString()));
        }
    }
}
