//package com.hms.mybatis.tkmybatisdemo.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import tk.mybatis.spring.mapper.MapperScannerConfigurer;
//
///**
// * @author hms
// * @date 2018/12/26 15:24
// */
//@Configuration
//public class MyBatisMapperScannerConfig {
//    @Bean
//    public MapperScannerConfigurer mapperScannerConfigurer() {
//        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
//        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
//        //扫描该路径下的dao
//        mapperScannerConfigurer.setBasePackage("com.hms.mybatis.tkmybatisdemo.mapper");
////        Properties properties = new Properties();
////        //通用dao
////        properties.setProperty("mappers", "tk.mybatis.mapper.common.Mapper");
////        properties.setProperty("notEmpty", "true");
////        properties.setProperty("IDENTITY", "MYSQL");
////        mapperScannerConfigurer.setProperties(properties);
//        return mapperScannerConfigurer;
//    }
//}
