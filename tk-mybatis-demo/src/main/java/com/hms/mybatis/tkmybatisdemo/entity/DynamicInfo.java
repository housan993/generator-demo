package com.hms.mybatis.tkmybatisdemo.entity;

import com.hms.mybatis.tkmybatisdemo.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "dynamic_info")
public class DynamicInfo extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 关系id
     */
    @Column(name = "rel_id")
    private Integer relId;

    /**
     * 状态id
     */
    @Column(name = "status_id")
    private Integer statusId;

    /**
     * 动态字段名称
     */
    @Column(name = "dynamic_name")
    private String dynamicName;

    /**
     * 动态字段值
     */
    @Column(name = "dynamic_value")
    private String dynamicValue;

    /**
     * 创建时间
     */
    @Column(name = "creation_date")
    private Date creationDate;

    /**
     * 修改时间
     */
    @Column(name = "modified_date")
    private Date modifiedDate;

    /**
     * 创建人id
     */
    @Column(name = "founder_id")
    private Integer founderId;

    /**
     * 修改人id
     */
    @Column(name = "modifier_id")
    private Integer modifierId;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取关系id
     *
     * @return rel_id - 关系id
     */
    public Integer getRelId() {
        return relId;
    }

    /**
     * 设置关系id
     *
     * @param relId 关系id
     */
    public void setRelId(Integer relId) {
        this.relId = relId;
    }

    /**
     * 获取状态id
     *
     * @return status_id - 状态id
     */
    public Integer getStatusId() {
        return statusId;
    }

    /**
     * 设置状态id
     *
     * @param statusId 状态id
     */
    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    /**
     * 获取动态字段名称
     *
     * @return dynamic_name - 动态字段名称
     */
    public String getDynamicName() {
        return dynamicName;
    }

    /**
     * 设置动态字段名称
     *
     * @param dynamicName 动态字段名称
     */
    public void setDynamicName(String dynamicName) {
        this.dynamicName = dynamicName;
    }

    /**
     * 获取动态字段值
     *
     * @return dynamic_value - 动态字段值
     */
    public String getDynamicValue() {
        return dynamicValue;
    }

    /**
     * 设置动态字段值
     *
     * @param dynamicValue 动态字段值
     */
    public void setDynamicValue(String dynamicValue) {
        this.dynamicValue = dynamicValue;
    }

    /**
     * 获取创建时间
     *
     * @return creation_date - 创建时间
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * 设置创建时间
     *
     * @param creationDate 创建时间
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * 获取修改时间
     *
     * @return modified_date - 修改时间
     */
    public Date getModifiedDate() {
        return modifiedDate;
    }

    /**
     * 设置修改时间
     *
     * @param modifiedDate 修改时间
     */
    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    /**
     * 获取创建人id
     *
     * @return founder_id - 创建人id
     */
    public Integer getFounderId() {
        return founderId;
    }

    /**
     * 设置创建人id
     *
     * @param founderId 创建人id
     */
    public void setFounderId(Integer founderId) {
        this.founderId = founderId;
    }

    /**
     * 获取修改人id
     *
     * @return modifier_id - 修改人id
     */
    public Integer getModifierId() {
        return modifierId;
    }

    /**
     * 设置修改人id
     *
     * @param modifierId 修改人id
     */
    public void setModifierId(Integer modifierId) {
        this.modifierId = modifierId;
    }

	/*CUSTOM_CODE_START*/

	/*CUSTOM_CODE_END*/
}