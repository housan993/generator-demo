package com.hms.mybatis.tkmybatisdemo.mapper;

import com.hms.mybatis.tkmybatisdemo.entity.DynamicInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface DynamicInfoMapper extends Mapper<DynamicInfo> {

	/*CUSTOM_CODE_START*/

    /**
     *
     * @param relId
     * @param statusId
     * @return
     */
    @Select("SELECT * from dynamic_info WHERE rel_id = #{relId} and status_id = #{statusId} ")
    List<DynamicInfo> selectByRelIdAndStatusId(@Param("relId") Integer relId,
                                               @Param("statusId") Integer statusId);
	/*CUSTOM_CODE_END*/
}