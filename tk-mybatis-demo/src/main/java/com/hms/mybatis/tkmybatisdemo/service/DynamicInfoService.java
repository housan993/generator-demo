package com.hms.mybatis.tkmybatisdemo.service;


import com.hms.mybatis.tkmybatisdemo.entity.DynamicInfo;

import java.util.List;

/**
 * 动态字段存储服务接口
 * @author hms
 * @date 2018/11/5 14:15
 */
public interface DynamicInfoService {

    /**
     * 插入动态字段信息
     * @param dynamicInfo
     */
    void insertDynamicInfo(DynamicInfo dynamicInfo);

    /**
     * 插入动态数据
     * @param dynamicInfoList
     */
    void insertDynamicInfoList(List<DynamicInfo> dynamicInfoList);

    /**
     * 根据关系id和状态id查询动态字段信息
     * @param relId
     * @param statusId
     * @return
     */
    List<DynamicInfo> selectByRelIdAndStatusId(Integer relId, Integer statusId);

    List<DynamicInfo> selectByRelIdAndStatusId1(Integer relId, Integer statusId);
}
