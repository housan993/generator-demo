package com.hms.mybatis.tkmybatisdemo.service.impl;


import com.hms.mybatis.tkmybatisdemo.entity.DynamicInfo;
import com.hms.mybatis.tkmybatisdemo.mapper.DynamicInfoMapper;
import com.hms.mybatis.tkmybatisdemo.service.DynamicInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 动态字段服务
 *
 * @author hms
 * @date 2018/11/5 14:45
 */
@Service
public class DynamicInfoServiceImpl implements DynamicInfoService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private Environment environment;

    @Resource
    private DynamicInfoMapper dynamicInfoMapper;

    @Override
    public void insertDynamicInfo(DynamicInfo dynamicInfo) {
        dynamicInfoMapper.insertSelective(dynamicInfo);
    }

    @Override
    public void insertDynamicInfoList(List<DynamicInfo> dynamicInfoList) {
        for (DynamicInfo dynamicInfo : dynamicInfoList) {
            dynamicInfoMapper.insertSelective(dynamicInfo);
        }
    }

    @Override
    public List<DynamicInfo> selectByRelIdAndStatusId(Integer relId, Integer statusId) {
        DynamicInfo dynamicInfo = new DynamicInfo();
        dynamicInfo.setRelId(relId);
        dynamicInfo.setStatusId(statusId);
        return dynamicInfoMapper.select(dynamicInfo);
    }

    @Override
    public List<DynamicInfo> selectByRelIdAndStatusId1(Integer relId, Integer statusId) {
        return dynamicInfoMapper.selectByRelIdAndStatusId(relId, statusId);
    }
}
