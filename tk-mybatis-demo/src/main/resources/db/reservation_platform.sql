
DROP TABLE IF EXISTS `dynamic_info`;
CREATE TABLE `dynamic_info`  (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `rel_id` int(50) NULL DEFAULT NULL COMMENT '关系id',
  `status_id` int(50) NULL DEFAULT NULL COMMENT '状态id',
  `dynamic_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '动态字段名称',
  `dynamic_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '动态字段值',
  `creation_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `modified_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `founder_id` int(50) NULL DEFAULT NULL COMMENT '创建人id',
  `modifier_id` int(50) NULL DEFAULT NULL COMMENT '修改人id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '被保人动态参数表' ROW_FORMAT = Dynamic;

INSERT INTO `dynamic_info` VALUES (15, 3, 21, 'extend', '{\"fromId\":\"oid\",\"aId\":1,\"userId\":87,\"projectId\":1}', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (16, 3, 21, 'channelSourceKey', '\"993\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (17, 3, 21, 'reservationStatusId', '21', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (18, 3, 21, 'channelSourceType', '\"hms\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (19, 3, 22, 'extend', '{\"fromId\":\"oid\",\"aId\":1,\"userId\":87,\"projectId\":1}', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (20, 3, 22, 'channelSourceKey', '\"993\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (21, 3, 22, 'reservationStatusId', '22', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (22, 3, 22, 'channelSourceType', '\"hms\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (23, 3, 23, 'extend', '{\"fromId\":\"oid\",\"aId\":1,\"userId\":87,\"projectId\":1}', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (24, 3, 23, 'channelSourceKey', '\"993\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (25, 3, 23, 'reservationStatusId', '23', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (26, 3, 23, 'channelSourceType', '\"hms\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (27, 3, 24, 'extend', '{\"fromId\":\"oid\",\"aId\":1,\"userId\":87,\"projectId\":1}', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (28, 3, 24, 'channelSourceKey', '\"993\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (29, 3, 24, 'reservationStatusId', '24', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (30, 3, 24, 'channelSourceType', '\"\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (31, 3, 25, 'extend', '{\"fromId\":\"o8jhX6Ey7Fkvcdy1qFwT9XHIOF4g\",\"aId\":2,\"userId\":87,\"projectId\":6}', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (32, 3, 25, 'channelSourceKey', '\"993\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (33, 3, 25, 'reservationStatusId', '25', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (34, 3, 25, 'channelSourceType', '\"wechat\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (35, 3, 26, 'extend', '{\"fromId\":\"o8jhX6Ey7Fkvcdy1qFwT9XHIOF4g\",\"aId\":1,\"userId\":87,\"projectId\":1}', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (36, 3, 26, 'channelSourceKey', '\"993\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (37, 3, 26, 'reservationStatusId', '26', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (38, 3, 26, 'channelSourceType', '\"wechat\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (39, 3, 27, 'extend', '{\"fromId\":\"o8jhX6Ey7Fkvcdy1qFwT9XHIOF4g\",\"aId\":1,\"userId\":87,\"projectId\":1}', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (40, 3, 27, 'channelSourceKey', '\"993\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (41, 3, 27, 'reservationStatusId', '27', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (42, 3, 27, 'channelSourceType', '\"wechat\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (43, 3, 28, 'extend', '{\"fromId\":\"o8jhX6Ey7Fkvcdy1qFwT9XHIOF4g\",\"aId\":1,\"userId\":88,\"projectId\":1}', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (44, 3, 28, 'channelSourceKey', '\"993\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (45, 3, 28, 'reservationStatusId', '28', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (46, 3, 28, 'channelSourceType', '\"wechat\"', '2018-12-27 16:42:35', '2018-12-27 16:42:35', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (47, 3, 29, 'extend', '{\"fromId\":\"oid\",\"aId\":1,\"userId\":87,\"projectId\":1}', '2018-12-27 16:50:53', '2018-12-27 16:50:53', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (48, 3, 29, 'channelSourceKey', '\"993\"', '2018-12-27 16:50:53', '2018-12-27 16:50:53', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (49, 3, 29, 'reservationStatusId', '29', '2018-12-27 16:50:53', '2018-12-27 16:50:53', NULL, NULL);
INSERT INTO `dynamic_info` VALUES (50, 3, 29, 'channelSourceType', '\"\"', '2018-12-27 16:50:53', '2018-12-27 16:50:53', NULL, NULL);

